import firebase from 'firebase/app'

const firebaseMixin= {
    created() {
        this.firebaseApp=firebase.initializeApp(firebaseConfig)
    },
    data() {
        return {
            firebaseApp:{},
            firebaseConfig : {
                apiKey: "AIzaSyDCNqU-6JLkS62HVYZKBkU8e3xabk8aBu0",
                authDomain: "primer-ciined.firebaseapp.com",
                databaseURL: "https://primer-ciined.firebaseio.com",
                projectId: "primer-ciined",
                storageBucket: "primer-ciined.appspot.com",
                messagingSenderId: "639487160758",
                appId: "1:639487160758:web:99553a946ca2eec8e8d117",
                measurementId: "G-12GVS63WEN"
              }
        }
    },
}

Vue.mixin(firebaseMixin)