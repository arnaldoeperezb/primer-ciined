import { vuexfireMutations, firestoreAction } from 'vuexfire'
import firebase from '../../firebaseInit'
import 'firebase/firestore'

// Get a Firestore instance
const db = firebase.firestore()

export default{
  mutations: {
    // other mutations
    ...vuexfireMutations,
  },
  state: {
      index:{},
  },
  actions: {
    bindIndexRef: firestoreAction(context => {
        // context contains all original properties like commit, state, etc
        // and adds `bindFirestoreRef` and `unbindFirestoreRef`
        // we return the promise returned by `bindFirestoreRef` that will
        // resolve once data is ready
        return context.bindFirestoreRef('index', db.collection('index'))
      })
    }
}
