import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#ff5722',
        secondary: '#3f51b5',
        accent: '#673ab7',
        error: '#f44336',
        warning: '#ff9800',
        info: '#00bcd4',
        success: '#4caf50',
        chatAnfitrion: '#66BB6A', //'green lighten-1',
        chatUsuario: '#29B6F6', //'light-blue lighten-1',
        chatOtros: '#FF7043', //'deep-orange lighten-1'
      },
      dark: {
        primary: '#ff5722',
        secondary: '#3f51b5',
        accent: '#673ab7',
        error: '#f44336',
        warning: '#ff9800',
        info: '#00bcd4',
        success: '#4caf50',
        chatAnfitrion: '#43A047', //'green darken-1',
        chatUsuario: '#01579B', //'light-blue darken-4',
        chatOtros: '#F4511E', //'deep-orange darken-1'
      },
    },
  }
});
